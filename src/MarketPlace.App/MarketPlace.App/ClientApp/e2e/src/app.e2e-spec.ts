// ====================================================
// More Templates: https://www.ebenmonney.com/templates
// Email: support@ebenmonney.com
// ====================================================

import { AppPage } from './app.po';

describe('MarketPlace.App App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display application title: MarketPlace.App', () => {
    page.navigateTo();
    expect(page.getAppTitle()).toEqual('MarketPlace.App');
  });
});
