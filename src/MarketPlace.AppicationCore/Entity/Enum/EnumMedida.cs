﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MarketPlace.AppicationCore.Entity.Enum
{
    public enum EnumMedida
    {
        [Display(Name = "Unidade")]
        Unidade,
        [Display(Name = "Litros")]
        Litro,
        [Display(Name = "Quilos")]
        Kg
    }
}
