﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MarketPlace.AppicationCore.Entity.Enum
{
    public enum EnumDesconto
    {
        [Display(Name = "%")]
        percentual,
        [Display(Name = "R$")]
        dinheiro
    }
}
