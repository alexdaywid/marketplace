﻿using MarketPlace.AppicationCore.Entity.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketPlace.AppicationCore.Entity
{
    public class Produto
    {
        public int ProdutoId { get; set; }

        public string Codigo { get; set; }

        public string Descricao { get; set; }

        public DateTime DataCadastro { get; set; }
        
        public float ValorCusto { get; set; }

        public float ValorVenda { get; set; }

        public float Desconto { get; set; }

        public EnumDesconto TipoDesconto { get; set; }

        public ICollection<Insumo> Insumo { get; set; }

        public int FornecedorId { get; set; }

        public Fornecedor Fornecedor { get; set; }

    }
}
