﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MarketPlace.AppicationCore.Entity
{
    public class Fornecedor
    {
        public int FornecedorId { get; set; }

        public string Codigo { get; set; }

        public int PessoaId { get; set; }

        public Pessoa Pessoa { get; set; }

        
    }
}
