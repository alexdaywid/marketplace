﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarketPlace.AppicationCore.Entity
{
    public class Servico
    {
        public int ServicoId { get; set; }

        public string Codigo { get; set; }

        public string Descricao { get; set; }

        public string ValorCusto { get; set; }

        public string ValorVenda { get; set; }

        public string Observação { get; set; }

    }
}
