﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MarketPlace.AppicationCore.Entity
{
    public class Pessoa
    {
        public int PessoaId { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string TelefoneCelular {get; set;}

        public string TelefoneFixo { get; set; }

        public int TipoPessoa { get; set; }

        public Cliente Cliente { get; set; }

        public ICollection<Endereco> Endereco { get; set; }

        [NotMapped]
        public string Cpf { get; set; }

        [NotMapped]
        public string Rg { get; set; }

        [NotMapped]
        public DateTime DtNascimento { get; set; }

        [NotMapped]
        public string NomeFantasia { get; set; }

        [NotMapped]
        public string Cnpj { get; set; }

        [NotMapped]
        public string InscMunicipal { get; set; }

        [NotMapped]
        public string InscEstadual { get; set; }

        [NotMapped]
        public string DtFundacao { get; set; }
    }

}
