﻿using MarketPlace.AppicationCore.Entity.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketPlace.AppicationCore.Entity
{
    public class Insumo
    {
        public int InsumoId { get; set; }

        public string Descricao { get; set; }

        public int Valor { get; set; }

        public int Quantidade { get; set; }

      //  public EnumMedida Medida{ get; set; }
    }
}
