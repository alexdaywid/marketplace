﻿using MarketPlace.AppicationCore.Entity;
using MarketPlace.AppicationCore.Interfaces;
using MarketPlace.AppicationCore.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace MarketPlace.AppicationCore.Service
{
    public class ForncedorService : IFornecedorService
    {
        public readonly IUnitOfWork _uow;

        public ForncedorService(IUnitOfWork uow)
        {
            this._uow = uow;
        }

        public Fornecedor Add(Fornecedor fornecedor)
        {
            Fornecedor f = _uow.Fornecedor.Add(fornecedor);
            _uow.Commit();

            return f;
        }

        public IEnumerable<Fornecedor> Find(Expression<Func<Fornecedor, bool>> predicado)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Fornecedor> GetAll()
        {
            return _uow.Fornecedor.GetAll();
        }

        public Fornecedor GetId(int? id)
        {
            throw new NotImplementedException();
        }

        public void Remove(Fornecedor entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Fornecedor entity)
        {
            throw new NotImplementedException();
        }
    }
}
