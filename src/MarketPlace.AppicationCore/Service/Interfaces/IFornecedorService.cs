﻿using MarketPlace.AppicationCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace MarketPlace.AppicationCore.Service.Interfaces
{
    public interface IFornecedorService
    {
        Fornecedor Add(Fornecedor fornecedor);

        void Remove(Fornecedor entity);

        void Update(Fornecedor entity);

        IEnumerable<Fornecedor> GetAll();

        Fornecedor GetId(int? id);

        IEnumerable<Fornecedor> Find(Expression<Func<Fornecedor, bool>> predicado);
    }
}
