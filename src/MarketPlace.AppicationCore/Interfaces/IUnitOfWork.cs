﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarketPlace.AppicationCore.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IClienteRepository Cliente { get; }
        IEnderecoRepository Endereco { get; }
        IPessoaRepository Pessoa { get; }
        IFornecedorRepository Fornecedor { get;}
        int Commit();
    }
}
