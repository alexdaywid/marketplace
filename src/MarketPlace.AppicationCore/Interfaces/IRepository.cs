﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace MarketPlace.AppicationCore.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Add(TEntity entity);
        
        void Remove(TEntity entity);

        void Update(TEntity entity);

        IEnumerable<TEntity> GetAll();

        TEntity GetId(int? id);

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicado);
    }
}
