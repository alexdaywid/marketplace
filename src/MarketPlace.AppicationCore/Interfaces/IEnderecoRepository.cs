﻿using MarketPlace.AppicationCore.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketPlace.AppicationCore.Interfaces
{
    public interface IEnderecoRepository : IRepository<Endereco>
    {

    }
}
