﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketPlace.Infrastruture.Migrations
{
    public partial class AlteracaoCliente : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pessoa_Cliente_ClienteId",
                table: "Pessoa");

            migrationBuilder.DropIndex(
                name: "IX_Pessoa_ClienteId",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "ClienteId",
                table: "Pessoa");

            migrationBuilder.AddColumn<int>(
                name: "PessoaId",
                table: "Cliente",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Cliente_PessoaId",
                table: "Cliente",
                column: "PessoaId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Cliente_Pessoa_PessoaId",
                table: "Cliente",
                column: "PessoaId",
                principalTable: "Pessoa",
                principalColumn: "PessoaId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cliente_Pessoa_PessoaId",
                table: "Cliente");

            migrationBuilder.DropIndex(
                name: "IX_Cliente_PessoaId",
                table: "Cliente");

            migrationBuilder.DropColumn(
                name: "PessoaId",
                table: "Cliente");

            migrationBuilder.AddColumn<int>(
                name: "ClienteId",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Pessoa_ClienteId",
                table: "Pessoa",
                column: "ClienteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Pessoa_Cliente_ClienteId",
                table: "Pessoa",
                column: "ClienteId",
                principalTable: "Cliente",
                principalColumn: "ClienteId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
