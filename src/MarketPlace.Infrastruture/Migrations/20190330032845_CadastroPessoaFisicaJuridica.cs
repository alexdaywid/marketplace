﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketPlace.Infrastruture.Migrations
{
    public partial class CadastroPessoaFisicaJuridica : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pessoa_Pessoa_IdNavegationPessoaId",
                table: "Pessoa");

            migrationBuilder.DropForeignKey(
                name: "FK_Pessoa_Pessoa_PessoaJuridica_IdNavegationPessoaId",
                table: "Pessoa");

            migrationBuilder.DropIndex(
                name: "IX_Pessoa_IdNavegationPessoaId",
                table: "Pessoa");

            migrationBuilder.DropIndex(
                name: "IX_Pessoa_PessoaJuridica_IdNavegationPessoaId",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "IdNavegationPessoaId",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "PessoaJuridica_IdNavegationPessoaId",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "Teste",
                table: "Pessoa");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdNavegationPessoaId",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PessoaJuridica_IdNavegationPessoaId",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Teste",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Pessoa_IdNavegationPessoaId",
                table: "Pessoa",
                column: "IdNavegationPessoaId");

            migrationBuilder.CreateIndex(
                name: "IX_Pessoa_PessoaJuridica_IdNavegationPessoaId",
                table: "Pessoa",
                column: "PessoaJuridica_IdNavegationPessoaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Pessoa_Pessoa_IdNavegationPessoaId",
                table: "Pessoa",
                column: "IdNavegationPessoaId",
                principalTable: "Pessoa",
                principalColumn: "PessoaId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Pessoa_Pessoa_PessoaJuridica_IdNavegationPessoaId",
                table: "Pessoa",
                column: "PessoaJuridica_IdNavegationPessoaId",
                principalTable: "Pessoa",
                principalColumn: "PessoaId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
