﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketPlace.Infrastruture.Migrations
{
    public partial class AlterarEndereco : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Endereco_Cliente_ClienteId",
                table: "Endereco");

            migrationBuilder.RenameColumn(
                name: "ClienteId",
                table: "Endereco",
                newName: "PessoaId");

            migrationBuilder.RenameIndex(
                name: "IX_Endereco_ClienteId",
                table: "Endereco",
                newName: "IX_Endereco_PessoaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Endereco_Pessoa_PessoaId",
                table: "Endereco",
                column: "PessoaId",
                principalTable: "Pessoa",
                principalColumn: "PessoaId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Endereco_Pessoa_PessoaId",
                table: "Endereco");

            migrationBuilder.RenameColumn(
                name: "PessoaId",
                table: "Endereco",
                newName: "ClienteId");

            migrationBuilder.RenameIndex(
                name: "IX_Endereco_PessoaId",
                table: "Endereco",
                newName: "IX_Endereco_ClienteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Endereco_Cliente_ClienteId",
                table: "Endereco",
                column: "ClienteId",
                principalTable: "Cliente",
                principalColumn: "ClienteId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
