﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketPlace.Infrastruture.Migrations
{
    public partial class AlteraFornecedor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "Cpf",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "DtNascimento",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "Rg",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "Cnpj",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "DtFundacao",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "InscEstadual",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "InscMunicipal",
                table: "Pessoa");

            migrationBuilder.DropColumn(
                name: "NomeFantasia",
                table: "Pessoa");

            migrationBuilder.AddColumn<int>(
                name: "tipo",
                table: "Fornecedor",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "tipo",
                table: "Fornecedor");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Pessoa",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Cpf",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DtNascimento",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Rg",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Cnpj",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DtFundacao",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InscEstadual",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InscMunicipal",
                table: "Pessoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NomeFantasia",
                table: "Pessoa",
                nullable: true);
        }
    }
}
