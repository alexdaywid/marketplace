﻿using MarketPlace.AppicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MarketPlace.Infrastruture.Data.EnderecoRepository
{
    public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Context;
        internal DbSet<TEntity> _dbSet;

        public EFRepository(DbContext context)
        {
            Context = context;
            _dbSet = context.Set<TEntity>();
        }


        public virtual IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public virtual void Update(TEntity entity)
        {
            _dbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public virtual IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public virtual TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.SingleOrDefault(predicate);
        }

      

        public virtual void Remove(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public virtual void RemoveRange(IEnumerable<TEntity> entities)
        {
            _dbSet.RemoveRange(entities);
        }

        public virtual TEntity GetId(int? id)
        {
            // aqui estamos trabalhando com o DbContext, e não com ArtistaContext 
            // assim não temos objteos DBSet
            // como Artistas e ArtistaDetalhes, e precisamos usar um método
            // genérico Set() para fazer o acesso
            return _dbSet.Find(id);
        }

        public virtual TEntity Add(TEntity entity)
        {
           return _dbSet.Add(entity).Entity;
        }
    }
}
