﻿using MarketPlace.AppicationCore.Entity;
using MarketPlace.AppicationCore.Interfaces;
using MarketPlace.Infrastruture.Data.EnderecoRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketPlace.Infrastruture.Data.Repository
{
    public class PessoaRepository : EFRepository<Pessoa>, IPessoaRepository
    {
        public PessoaRepository(MarketPlaceDBcontext dbContext) : base(dbContext)
        {

        }


        public MarketPlaceDBcontext MarketPlaceDBcontext
        {
            get { return Context as MarketPlaceDBcontext; }
        }



    }
}
