﻿using MarketPlace.AppicationCore.Entity;
using MarketPlace.AppicationCore.Interfaces;
using MarketPlace.Infrastruture.Data.EnderecoRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarketPlace.Infrastruture.Data.Repository
{
    public class FornecedorRepository: EFRepository<Fornecedor>, IFornecedorRepository
    {
        

        public FornecedorRepository(MarketPlaceDBcontext dbContext) : base(dbContext)
        {
           
        }

        public override IEnumerable<Fornecedor> GetAll()
        {
            var fornecedores = MarketPlaceDBcontext.Fornecedores
                               .Include(x => x.Pessoa)
                               .ThenInclude(x => x.Endereco).ToList();
                   

            return fornecedores;
        }

        public override Fornecedor Add(Fornecedor entity)
        {
          
            return base.Add(entity);
        }

        public MarketPlaceDBcontext MarketPlaceDBcontext
        {
            get { return Context as MarketPlaceDBcontext; }
        }
    }
}
