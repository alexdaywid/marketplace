﻿using MarketPlace.AppicationCore.Interfaces;
using MarketPlace.Infrastruture.Data.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketPlace.Infrastruture.Data.EnderecoRepository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MarketPlaceDBcontext _context;

        public UnitOfWork(MarketPlaceDBcontext context)
        {
            _context = context;
            Cliente = new ClienteRepository(_context);
            Endereco = new EnderecoRepository(_context);
            Pessoa = new PessoaRepository(_context);
            Fornecedor = new FornecedorRepository(_context);
           
        }

        public IClienteRepository Cliente { get; private set; }
        public IEnderecoRepository Endereco { get; private set; }
        public IPessoaRepository Pessoa { get; private set; }
        public IFornecedorRepository Fornecedor { get; private set; }
      

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
