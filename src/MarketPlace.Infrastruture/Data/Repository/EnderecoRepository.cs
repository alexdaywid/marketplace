﻿using MarketPlace.AppicationCore.Entity;
using MarketPlace.AppicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketPlace.Infrastruture.Data.EnderecoRepository
{
    public class EnderecoRepository : EFRepository<Endereco> , IEnderecoRepository
    {
        public EnderecoRepository(MarketPlaceDBcontext dbContext) : base(dbContext)
        {

        }

        public MarketPlaceDBcontext MarketPlaceDBcontext
        {
            get { return Context as MarketPlaceDBcontext; }
        }
    }
}
