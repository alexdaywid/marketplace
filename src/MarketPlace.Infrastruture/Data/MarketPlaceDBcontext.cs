﻿using MarketPlace.AppicationCore.Entity;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Infrastruture.Data
{
    public class MarketPlaceDBcontext : DbContext
    {
        public MarketPlaceDBcontext(DbContextOptions<MarketPlaceDBcontext> options) : base(options)
        {

        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }
        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<PessoaFisica> PessoaFisicas { get; set; }
        public DbSet<PessoaJuridica> PessoaJuridicas { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Servico> Servicos { get; set; }
        public DbSet<Insumo> Insumos { get; set; }
        public DbSet<Fornecedor> Fornecedores { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Cliente>().ToTable("Cliente");
            builder.Entity<Endereco>().ToTable("Endereco");
            builder.Entity<Pessoa>().ToTable("Pessoa");
            builder.Entity<Produto>().ToTable("Produto");
            builder.Entity<Servico>().ToTable("Servico");
            builder.Entity<Insumo>().ToTable("Insumo");
            builder.Entity<Fornecedor>().ToTable("Fornecedor");
        }
    }
}
