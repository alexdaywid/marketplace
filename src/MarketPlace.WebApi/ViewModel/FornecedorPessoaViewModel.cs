﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.WebApi.ViewModel
{
    public class FornecedorPessoaViewModel
    {
        public int FornecedorId { get; set; }

        public string Codigo { get; set; }

        public bool Tipo { get; set; }

        public PessoaViewModel Pessoa { get; set; }
    }
}
