﻿using MarketPlace.AppicationCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.WebApi.ViewModel
{
    public class FornecedorViewModel
    {
        public int FornecedorId { get; set; }

        public string Codigo { get; set; }

        public bool Tipo { get; set; }

        public PessoaViewModel Pessoa { get; set; }

        public string Cpf { get; set; }

        public string Rg { get; set; }

        public DateTime DtNascimento { get; set; }

        public string NomeFantasia { get; set; }

        public string Cnpj { get; set; }

        public string InscMunicipal { get; set; }

        public string InscEstadual { get; set; }

        public string DtFundacao { get; set; }
    }
}
