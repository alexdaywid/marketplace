﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.WebApi.ViewModel
{
    public class PessoaViewModel
    {
        public int PessoaId { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string TelefoneCelular { get; set; }

        public string TelefoneFixo { get; set; }

        public int TipoPessoa { get; set; }

        [NotMapped]
        public string Cpf { get; set; }

        [NotMapped]
        public string Rg { get; set; }

        [NotMapped]
        public DateTime DtNascimento { get; set; }

        [NotMapped]
        public string NomeFantasia { get; set; }

        [NotMapped]
        public string Cnpj { get; set; }

        [NotMapped]
        public string InscMunicipal { get; set; }

        [NotMapped]
        public string InscEstadual { get; set; }

        [NotMapped]
        public string DtFundacao { get; set; }

        public ICollection<EnderecoViewModel> Endereco { get; set; }
    }
}
