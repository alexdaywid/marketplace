﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MarketPlace.AppicationCore.Entity;
using MarketPlace.AppicationCore.Interfaces;
using MarketPlace.AppicationCore.Service.Interfaces;
using MarketPlace.WebApi.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarketPlace.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FornecedorController : ControllerBase
    {
        private readonly IFornecedorService _fornecedorService;
        private readonly IMapper _mapper;

        public FornecedorController(IFornecedorService fornecedorService, IMapper mapper)
        {
            this._fornecedorService = fornecedorService;
            this._mapper = mapper;
        }
        // GET: api/Fornecedor
        [HttpGet]
        public IEnumerable<FornecedorPessoaViewModel> GetAll()
        {
            var lstfornecedores = this._fornecedorService.GetAll();
            return _mapper.Map<IEnumerable<Fornecedor>, IEnumerable<FornecedorPessoaViewModel>>(lstfornecedores);            
        }

        // GET: api/Fornecedor/5
        [HttpGet("{id}", Name = "GetFornecedor")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Fornecedor
        [HttpPost]
        public IActionResult Post([FromBody] FornecedorPessoaViewModel fornecedorViewModel)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fornecedorPessoa = new Fornecedor();
            fornecedorPessoa = _mapper.Map<FornecedorPessoaViewModel,Fornecedor>(fornecedorViewModel);

            var fornecedor = this._fornecedorService.Add(fornecedorPessoa);

            return CreatedAtRoute("GetFornecedor", new { id = fornecedor.FornecedorId }, fornecedorViewModel);

        }

        // PUT: api/Fornecedor/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
