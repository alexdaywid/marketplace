﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.AppicationCore.Entity;
using MarketPlace.AppicationCore.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarketPlace.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnderecoController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public EnderecoController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Endereco
        [HttpGet]
        public IEnumerable<Endereco> Get()
        {
            return _unitOfWork.Endereco.GetAll();
        }

        // GET: api/Endereco/5
        [HttpGet("{id}", Name = "Get")]
        public ActionResult<Endereco>  Get(int? id)
        {
            var endereco =  _unitOfWork.Endereco.GetId(id);
            if (!ModelState.IsValid)
            {
                return NotFound();
            }
            return Ok(endereco);
        }

        // POST: api/Endereco
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Endereco/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
