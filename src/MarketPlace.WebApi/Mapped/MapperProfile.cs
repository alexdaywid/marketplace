﻿using AutoMapper;
using MarketPlace.AppicationCore.Entity;
using MarketPlace.WebApi.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.WebApi.Mapped
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Fornecedor, FornecedorViewModel>().ReverseMap();
            CreateMap<Fornecedor, FornecedorPessoaViewModel>().ReverseMap();
            CreateMap<PessoaJuridica, PessoaJuridicaViewModel>().ReverseMap();
            CreateMap<PessoaFisica, PessoaFisicaViewModel>().ReverseMap();
            CreateMap<Pessoa, PessoaViewModel>().ReverseMap();
            CreateMap<Endereco, EnderecoViewModel>().ReverseMap();
            CreateMap<Pessoa, PessoaViewModel >().ReverseMap();
        }
    }
}
