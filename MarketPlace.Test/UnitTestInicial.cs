﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using MarketPlace.Infrastruture;
using MarketPlace.AppicationCore;
using MarketPlace.Infrastruture.Data;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Infrastruture.Data.EnderecoRepository;
using MarketPlace.AppicationCore.Entity;
using System.Linq;
using MarketPlace.Infrastruture.Data.Repository;

namespace MarketPlace.Test
{
    public class UnitTestInicial
    {
        private readonly MarketPlaceDBcontext _dbContext;
        private readonly ClienteRepository clienteRepository;
        private readonly PessoaRepository pessoaRepository;
        //private readonly EnderecoRepository enderecoRepository;

        public UnitTestInicial()
        {
            var dbOptions = new DbContextOptionsBuilder<MarketPlaceDBcontext>()
                .UseSqlServer("Server=DESKTOP-AN2B5PC\\SQLEXPRESS;Database=Marketplace;Trusted_Connection=True;MultipleActiveResultSets=true")
                .Options;

            _dbContext = new MarketPlaceDBcontext(dbOptions);
            clienteRepository = new ClienteRepository(_dbContext);
            pessoaRepository = new PessoaRepository(_dbContext);
        }

        [Fact]
        public void TesteNewCliente()
        {
            // var clientes = clienteRepository.GetAll().Where(x => x.ClienteId == 1).Count();
            var pessoa = new PessoaFisica
            {
                Nome = "Paula",
                Email = "paula@gmail.com",
                TelefoneCelular = "3232323232",
                TelefoneFixo = "",
                Cpf = "0587777722",
                Rg = "27012121",
                TipoPessoa = 1,
                DtNascimento = DateTime.Now
            };

            pessoaRepository.Add(pessoa);
            _dbContext.SaveChanges();

           // Assert.Equal(1,clientes);
        }
    }
}
